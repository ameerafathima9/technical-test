<?php 

class CalorificvalueModel extends CI_Model{

    public function save($data){
        $this->db->insert('calorific_values', $data);
    }

    public function getForAreaTest(){
        $this->db->select('calorific_values.applicable_for, calorific_values.calorific_value, ldz_areas.area_name');
        $this->db->from('calorific_values');
        $this->db->join('ldz_areas', 'calorific_values.area_id = ldz_areas.id', 'left');
        $this->db->order_by('calorific_values.applicable_for');
        $res = $this->db->get();
        if ($res->result_id->num_rows != 0) {
            return $res->result_array();
        } else {
            return [];
        }
    }


}

?>