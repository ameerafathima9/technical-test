<?php 

class LdzareaModel extends CI_Model{

    public function save($data){
        $this->db->insert('ldz_areas', $data);
    }

    public function get(){
        $res = $this->db->order_by('id')->get('ldz_areas');
        if ($res->result_id->num_rows != 0) {
            return $res->result_array();
        } else {
            return [];
        }
    }

}

?>