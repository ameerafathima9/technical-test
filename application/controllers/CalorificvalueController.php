<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class CalorificvalueController extends CI_Controller {

   public function __construct()
   {
      parent::__construct();
      $this->load->model('CalorificvalueModel');
      $this->load->model('LdzareaModel');
   }

   public function calorificValues(){
      $calorific_values = $this->CalorificvalueModel->getForAreaTest();
      $this->load->view('template/header');
      $this->load->view('calorificvalue_view', ['calorific_values' => $calorific_values]);
      $this->load->view('template/footer');
   }

   public function fetchAndSaveData(){
      $ldz_areas = $this->fetchLDZAreas();
      foreach($ldz_areas as $ldz_area){
         $data = [
            'area_name' => trim(substr($ldz_area['name'], strpos($ldz_area['name'], ',') + 1)),
            'node_id' => $ldz_area['dataItemCategoryTreeNodeId'],
            'staging_id' => $ldz_area['stagingId'],
         ];
         $this->LdzareaModel->save($data);
      }
      $this->fetchAndSaveCalorificValues();
   }

   private function fetchLDZAreas(){
      $postfields = array();
      $postfields = 'PublicationObjectIds=408:28';
      $url = "http://mip-prd-web.azurewebsites.net/api/v2/DataItemCategoryTree";
      $response = $this->fetchCurlData($url, $postfields);
      $data_items = (array) json_decode($response, true);
      $calorific_value_item = false;
      $search = ['dataItemCategoryTreeNodeId' => '408'];
      foreach ($data_items as $value) {
          if ($value['dataItemCategoryTreeNodeId'] == $search['dataItemCategoryTreeNodeId']) {
              $calorific_value_item = $value;
              break;
          }
      }
      $ldz_areas = $calorific_value_item ? $calorific_value_item['children'] : null;
      return $ldz_areas;
   }

   private function fetchAndSaveCalorificValues(){
      $areas = $this->LdzareaModel->get();
      foreach($areas as $ldz_area){
         $postfield = array();
         $postfield['LatestValue'] = 'true';
         $postfield['PublicationObjectIds'] = '408:'.$ldz_area['node_id'];
         $postfield['PublicationObjectStagingIds'] = $ldz_area['staging_id'];
         $postfield['Applicable'] = 'applicableFor';
         $postfield['PublicationObjectCount'] = '1';
         $postfield['FromUtcDatetime'] = '2021-01-01T00:00:00.000Z';
         $postfield['ToUtcDateTime'] = gmdate("Y/m/d H:i:s");
         $url = "http://mip-prd-web.azurewebsites.net/DataItemViewer/ViewReportDataItem";
         $response = $this->fetchCurlData($url, $postfield);
         $calorific_values = $this->htmlToArray($response);
         foreach($calorific_values as $item){
            $for_date = str_replace('/', '-', (string)$item['Applicable For']);
            $data = [
               'applicable_for' => date('Y-m-d', strtotime($for_date)),
               'calorific_value' => $item['Value'],
               'area_id' => $ldz_area['id'],
            ];
            $this->CalorificvalueModel->save($data);
         }
      }
   }

   public function fetchCurlData($url, $postfields){
      $curl = curl_init();
      curl_setopt($curl, CURLOPT_URL, $url);
      curl_setopt($curl, CURLOPT_POST, true);
      curl_setopt($curl, CURLOPT_POSTFIELDS, $postfields);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
      $response = curl_exec($curl);
      curl_close($curl);
      return $response;
   }

   public function htmlToArray($response){
      libxml_use_internal_errors(true);
      $DOM = new DOMDocument();
      $DOM->loadHTML($response);
      $header = $DOM->getElementsByTagName('th');
      $detail = $DOM->getElementsByTagName('td');
      foreach($header as $node_header) 
      {
         $table_header[] = trim($node_header->textContent);
      }
      $i = $j = 0;
      foreach($detail as $node_detail) 
      {
         $table_detail[$j][] = trim($node_detail->textContent);
         $i = $i + 1;
         $j = $i % count($table_header) == 0 ? $j + 1 : $j;
      }
      for($i = 0; $i < count($table_detail); $i++)
      {
         for($j = 0; $j < count($table_header); $j++)
         {
            $table_detail_array[$i][$table_header[$j]] = $table_detail[$i][$j];
         }
      }
      return $table_detail_array;
   }

}