
    <div class="container">
        <div class="row">
            <div class="col-md-12 mt-4">
                <div class="card">
                    <div class="card-header">
                        <h5>Calorific Values For LDZ Areas</h5>
                    </div>
                    <div class="card-body">
                        <table id="datatable1" class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Applicable For</th>
                                    <th>Calorific Value</th>
                                    <th>Area</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    foreach($calorific_values as $data){
                                ?>
                                <tr>
                                    <td><?php echo date('d/m/Y', strtotime($data['applicable_for'])) ?></td>
                                    <td><?php echo $data['calorific_value'] ?></td>
                                    <td><?php echo $data['area_name'] ?></td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    
